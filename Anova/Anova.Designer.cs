namespace Anova
{
    partial class Anova
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.btClear = new System.Windows.Forms.Button();
            this.btReset = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btProses = new System.Windows.Forms.Button();
            this.btInput = new System.Windows.Forms.Button();
            this.udKelompok = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btProses2 = new System.Windows.Forms.Button();
            this.dgData2 = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.udBlock = new System.Windows.Forms.NumericUpDown();
            this.udTreatment = new System.Windows.Forms.NumericUpDown();
            this.tFhitung = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udKelompok)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgData2)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTreatment)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(13, 13);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(339, 29);
            label1.TabIndex = 0;
            label1.Text = "Analysis of Varian (ANOVA)";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(15, 42);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(174, 13);
            label2.TabIndex = 1;
            label2.Text = "Aryo Pinandito / NRP. 9109205506";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(15, 22);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(31, 13);
            label3.TabIndex = 2;
            label3.Text = "Block";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(15, 25);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(57, 13);
            label4.TabIndex = 2;
            label4.Text = "Treatment";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(116, 25);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(31, 13);
            label5.TabIndex = 5;
            label5.Text = "Block";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.tbOutput);
            this.groupBox2.Controls.Add(this.btClear);
            this.groupBox2.Location = new System.Drawing.Point(420, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 288);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debug Message";
            // 
            // tbOutput
            // 
            this.tbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutput.Location = new System.Drawing.Point(6, 20);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(234, 233);
            this.tbOutput.TabIndex = 2;
            this.tbOutput.Text = "";
            this.tbOutput.WordWrap = false;
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(165, 259);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(75, 23);
            this.btClear.TabIndex = 1;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            // 
            // btReset
            // 
            this.btReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btReset.Location = new System.Drawing.Point(585, 26);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(75, 23);
            this.btReset.TabIndex = 4;
            this.btReset.Text = "Reset All";
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 71);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(402, 288);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(394, 262);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "   One-Way Anova   ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.tFhitung);
            this.groupBox4.Controls.Add(label6);
            this.groupBox4.Location = new System.Drawing.Point(6, 66);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(382, 190);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Analysis Result";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btProses);
            this.groupBox1.Controls.Add(this.btInput);
            this.groupBox1.Controls.Add(label3);
            this.groupBox1.Controls.Add(this.udKelompok);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 54);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // btProses
            // 
            this.btProses.Location = new System.Drawing.Point(301, 20);
            this.btProses.Name = "btProses";
            this.btProses.Size = new System.Drawing.Size(75, 23);
            this.btProses.TabIndex = 4;
            this.btProses.Text = "Proses";
            this.btProses.UseVisualStyleBackColor = true;
            this.btProses.Click += new System.EventHandler(this.btProses_Click);
            // 
            // btInput
            // 
            this.btInput.Location = new System.Drawing.Point(95, 19);
            this.btInput.Name = "btInput";
            this.btInput.Size = new System.Drawing.Size(87, 23);
            this.btInput.TabIndex = 3;
            this.btInput.Text = "Input Data";
            this.btInput.UseVisualStyleBackColor = true;
            this.btInput.Click += new System.EventHandler(this.btInput_Click);
            // 
            // udKelompok
            // 
            this.udKelompok.Location = new System.Drawing.Point(52, 20);
            this.udKelompok.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udKelompok.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udKelompok.Name = "udKelompok";
            this.udKelompok.Size = new System.Drawing.Size(37, 21);
            this.udKelompok.TabIndex = 0;
            this.udKelompok.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udKelompok.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(394, 262);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "   Two-Way Anova   ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btProses2);
            this.groupBox3.Controls.Add(this.dgData2);
            this.groupBox3.Location = new System.Drawing.Point(6, 66);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 190);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data";
            // 
            // btProses2
            // 
            this.btProses2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btProses2.Location = new System.Drawing.Point(289, 161);
            this.btProses2.Name = "btProses2";
            this.btProses2.Size = new System.Drawing.Size(87, 23);
            this.btProses2.TabIndex = 4;
            this.btProses2.Text = "Proses Data";
            this.btProses2.UseVisualStyleBackColor = true;
            this.btProses2.Click += new System.EventHandler(this.btProses2_Click);
            // 
            // dgData2
            // 
            this.dgData2.AllowUserToAddRows = false;
            this.dgData2.AllowUserToDeleteRows = false;
            this.dgData2.AllowUserToResizeColumns = false;
            this.dgData2.AllowUserToResizeRows = false;
            this.dgData2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgData2.BackgroundColor = System.Drawing.Color.White;
            this.dgData2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgData2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgData2.Location = new System.Drawing.Point(6, 20);
            this.dgData2.Name = "dgData2";
            this.dgData2.Size = new System.Drawing.Size(370, 135);
            this.dgData2.TabIndex = 0;
            this.dgData2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgData2_CellValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(label5);
            this.groupBox5.Controls.Add(this.udBlock);
            this.groupBox5.Controls.Add(label4);
            this.groupBox5.Controls.Add(this.udTreatment);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(382, 54);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Data Parameters";
            // 
            // udBlock
            // 
            this.udBlock.Location = new System.Drawing.Point(153, 23);
            this.udBlock.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udBlock.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udBlock.Name = "udBlock";
            this.udBlock.Size = new System.Drawing.Size(37, 21);
            this.udBlock.TabIndex = 4;
            this.udBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udBlock.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udBlock.ValueChanged += new System.EventHandler(this.udBlock_ValueChanged);
            // 
            // udTreatment
            // 
            this.udTreatment.Location = new System.Drawing.Point(73, 23);
            this.udTreatment.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udTreatment.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTreatment.Name = "udTreatment";
            this.udTreatment.Size = new System.Drawing.Size(37, 21);
            this.udTreatment.TabIndex = 0;
            this.udTreatment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udTreatment.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udTreatment.ValueChanged += new System.EventHandler(this.udTreatment_ValueChanged);
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(6, 26);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(48, 13);
            label6.TabIndex = 0;
            label6.Text = "F-Hitung";
            // 
            // tFhitung
            // 
            this.tFhitung.AutoSize = true;
            this.tFhitung.Location = new System.Drawing.Point(60, 26);
            this.tFhitung.Name = "tFhitung";
            this.tFhitung.Size = new System.Drawing.Size(13, 13);
            this.tFhitung.TabIndex = 1;
            this.tFhitung.Text = "0";
            // 
            // Anova
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 371);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btReset);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Anova";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analysis of Varian (ANOVA)";
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udKelompok)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgData2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udBlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTreatment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.Button btReset;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown udKelompok;
        private System.Windows.Forms.Button btInput;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown udTreatment;
        private System.Windows.Forms.DataGridView dgData2;
        private System.Windows.Forms.NumericUpDown udBlock;
        private System.Windows.Forms.RichTextBox tbOutput;
        private System.Windows.Forms.Button btProses2;
        private System.Windows.Forms.Button btProses;
        private System.Windows.Forms.Label tFhitung;

    }
}

