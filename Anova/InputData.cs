using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Anova
{
    public partial class InputData : Form
    {
        public InputData()
        {
            InitializeComponent();
        }

        public InputData(String nama, Double[] data)
        {
            InitializeComponent();
            List<String> DataString = new List<String>();
            foreach (Double d in data)
            {
                DataString.Add(d.ToString());
            }
            this.Text += " " + nama;
            this.tbNama.Text = nama;
            this.tbData.Text = String.Join("\r\n", DataString.ToArray());
        }

        public String Nama = "";
        public List<Double> Data = new List<Double>();

        private void btSubmit_Click(object sender, EventArgs e)
        {
            Boolean error = false;
            if (this.tbNama.Text.Trim() == "")
            {
                MessageBox.Show("Nama kelompok harus diisi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                error = true;
            }
            else if (this.tbData.Text.Trim() == "")
            {
                MessageBox.Show("Data harus diisi", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                error = true;
            }

            if (!error)
            {
                Int32 index = 0;
                this.Nama = this.tbNama.Text;
                String data = this.tbData.Text;
                try
                {
                    foreach (String d in data.Split(new String[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        Data.Add(Double.Parse(d));
                        index++;
                    }
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}