using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Anova
{
    public partial class Anova : Form
    {
        public Anova()
        {
            InitializeComponent();
            InitialValue();     // Set Default Value for One-Way Anova
            InitialValue2();    // Set Default Value for Two-Way Anova
        }

        #region Data One-Way
        // Data
        private List<Double[]> DefaultData = new List<Double[]>();
        private List<Double[]> Data = new List<Double[]>();
        private List<String> DefaultLabelData = new List<String>();
        private List<String> LabelData = new List<String>();

        private List<Double> MeanData = new List<Double>(); // Mean Data
        private Double GrandMean = 0; // Grand Mean
        private List<Double> Alpha = new List<Double>(); // Alpha
        private List<Double[]> Error = new List<Double[]>(); // Error

        // Square
        private List<Double[]> YMuSq = new List<Double[]>(); // Y-Mu Square
        private List<Double> AlphaSq = new List<Double>(); // Alpha Square
        private List<Double[]> ErrorSq = new List<Double[]>(); // Error Square

        // Sum Square
        private Double SSYMu    = 0; // Sum Square Y-Mu
        private Double SSAlpha  = 0; // Sum Square Alpha
        private Double SSError  = 0; // Sum Square Error

        // Degree of Freedom
        private Double DFYMu = 0;   // DF Y-Mu
        private Double DFAlpha = 0; // DF Alpha
        private Double DFError = 0; // DF Error

        // Mean Square
        private Double MSAlpha = 0; // Mean Square Alpha
        private Double MSError = 0; // Mean Square Error
        #endregion
        
        #region One-Way
        /// <summary>
        /// Method untuk inisialisasi nilai variabel data One-Way Anova
        /// </summary>
        private void InitialValue()
        {
            udKelompok.Value = 3;
            
            // Nilai default yang diberikan sebagai contoh
            DefaultData.Clear();
            DefaultData.Add(new Double[] { 70, 68, 58, 65, 75 });
            DefaultData.Add(new Double[] { 65, 62, 80 });
            DefaultData.Add(new Double[] { 71, 55, 52, 60 });

            // Nama blok kelompok data default yang diberikan sebagai contoh
            DefaultLabelData.Clear();
            DefaultLabelData.Add("A");
            DefaultLabelData.Add("B");
            DefaultLabelData.Add("C");

            // Data
            Data = new List<Double[]>();
            LabelData = new List<String>();

        }

        #region Input Data Tiap Blok
        /// <summary>
        /// Event ketika tombol Input Data di klik
        /// Untuk memasukkan nama dan nilai data tiap blok
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btInput_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();

            Int32 i = 0;
            for (i = 0; i < udKelompok.Value; i++)
            {
                InputData indata;
                try { indata = new InputData(DefaultLabelData[i], DefaultData[i]); }
                catch { indata = new InputData(); }

                // Tampilkan dialog box untuk memasukkan data
                DialogResult dr = indata.ShowDialog();
                // Jika data OK
                if (dr == DialogResult.OK)
                {
                    // Simpan label dan datanya
                    LabelData.Add(indata.Nama);
                    Data.Add(indata.Data.ToArray());
                    // Tampilkan datanya
                    Debug("Data " + LabelData[i], Data[i]);
                }
                else if (dr == DialogResult.Cancel) break;
            }
        }
        #endregion

        /// <summary>
        /// Event ketika tombol Proses diklik,
        /// proses analisis One-Way Anova dilakukan
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProses_Click(object sender, EventArgs e)
        {

            tbOutput.Clear();

            // Reset nilai variabel-variabel yang digunakan

            MeanData.Clear(); // Mean Data
            GrandMean = 0; // Grand Mean
            Alpha.Clear(); // Alpha
            Error.Clear(); // Error

            // Square
            YMuSq.Clear();
            AlphaSq.Clear(); // Alpha Square
            ErrorSq.Clear(); // Error Square

            // Sum Square
            SSYMu = 0;
            SSAlpha = 0;
            SSError = 0;

            // Degree of Freedom
            DFYMu = 0;
            DFAlpha = 0;
            DFError = 0;

            // Mean Square
            MSAlpha = 0;
            MSError = 0;

            // Tampilkan data untuk tiap blok 
            for (Int32 p = 0; p < udKelompok.Value; p++)
                Debug("Data " + LabelData[p], Data[p]);

            Int32 i = 0;
            Int32 j = 0;
            Int32 k = 0;
            Double sum = 0;
            Double sumGrand = 0;

            // Calculate Mean and Grand Mean
            foreach (Double[] da in Data)
            {
                sum = 0;
                foreach (Double d in da)
                {
                    sum += d;
                    sumGrand += d;
                    k++;
                }
                MeanData.Add((sum / da.Length));
                Debug("Mean Data " + LabelData[j], MeanData[j].ToString());
                j++;
            }

            // Calculate Grand Mean
            GrandMean = sumGrand / k;
            Debug("Grand Mean", String.Format("{0:0.#####}", GrandMean));

            // Calculate Alpha
            foreach (Double m in MeanData) Alpha.Add(m - GrandMean);
            Debug("Alpha", Alpha.ToArray());

            // Calculate Error
            i = 0;
            foreach (Double[] da in Data)
            {
                List<Double> errorkelompok = new List<Double>();
                for (j = 0; j < da.Length; j++)
                {
                    Double alpha = Alpha[i];
                    Double error = da[j] - (GrandMean + alpha);
                    errorkelompok.Add(error);
                    k = i + j;
                }
                Error.Add(errorkelompok.ToArray());
                Debug("Error " + LabelData[i], Error[i]);
                i++;
            }

            // Calculate Y-Mu Square
            i = 0;
            foreach (Double[] da in Data)
            {
                List<Double> ymusq = new List<Double>();
                for (j = 0; j < da.Length; j++)
                {
                    Double ymu = (da[j] - GrandMean);
                    ymusq.Add(ymu * ymu);
                    k = i + j;
                }
                YMuSq.Add(ymusq.ToArray());
                Debug("Y-Mu Square " + LabelData[i], YMuSq[i]);
                i++;
            }

            // Calculate Alpha Square
            foreach (Double d in Alpha) { AlphaSq.Add(d * d); }
            Debug("Alpha Square", AlphaSq.ToArray());

            // Calculate Error Square
            i = 0;
            foreach (Double[] er in Error)
            {
                List<Double> errorsq = new List<Double>();
                for (j = 0; j < er.Length; j++)
                {
                    errorsq.Add(er[j] * er[j]);
                }
                ErrorSq.Add(errorsq.ToArray());
                Debug("Error Square " + LabelData[i], ErrorSq[i]);
                i++;
            }

            // Calculate Sum Square Y-Mu
            i = 0;
            foreach (Double[] ymu in YMuSq)
            {
                for (j = 0; j < ymu.Length; j++) SSYMu += ymu[j];
                i++;
            }
            Debug("Sum Square Y-Mu ", SSYMu.ToString());

            // Calculate Sum Square Alpha
            for (i = 0; i < AlphaSq.Count; i++)
            {
                for (j = 0; j < Data[i].Length; j++) SSAlpha += AlphaSq[i];
            }
            Debug("Sum Square Alpha ", SSAlpha.ToString());

            // Calculate Sum Square Error
            i = 0;
            foreach (Double[] er in ErrorSq)
            {
                for (j = 0; j < er.Length; j++) SSError += er[j];
                i++;
            }
            Debug("Sum Square Error ", SSError.ToString());

            // Calculating Degree of Freedom Y-Mu
            foreach (Double[] d in Data) DFYMu += d.Length;
            DFYMu -= 1;
            Debug("Degree of Freedom Y-Mu ", DFYMu.ToString());

            // Calculating Degree of Freedom Alpha
            DFAlpha = Alpha.Count - 1;
            Debug("Degree of Freedom Alpha ", DFAlpha.ToString());

            // Calculating Degree of Freedom Error
            DFError = DFYMu - DFAlpha;
            Debug("Degree of Freedom Error ", DFError.ToString());

            // Calculating Mean Square
            MSAlpha = SSAlpha / DFAlpha;
            MSError = SSError / DFError;
            Debug("Mean Square Alpha ", MSAlpha.ToString());
            Debug("Mean Square Error ", MSError.ToString());

            // Calculating F Hitung
            Double FHitung = MSAlpha / MSError;
            Debug("F Hitung ", FHitung.ToString());

            // Tampilkan Nilai F Hitung dari blok
            tFhitung.Text = FHitung.ToString();
        }
        #endregion

        #region Debug
        /// <summary>
        /// Kumpulan method overload untuk menampilkan nilai-nilai 
        /// hasil analisis One-Way Anova dan Two-Way Anova
        /// </summary>
        private void Debug(String label, Double[] data)
        {
            List<String> DataString = new List<String>();
            foreach (Double d in data)
            {
                DataString.Add(String.Format("{0:0.#####}", d));
            }
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + String.Join(", ", DataString.ToArray()) + "\r\n\r\n";
        }

        private void Debug(String label, String data)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + data + "\r\n\r\n";
        }

        private void Debug(String label, Double data)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += " " + String.Format("{0:0.#####}",data) + "\r\n\r\n";
        }

        private void Debug(Matrix m, String label)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + label + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += m.ToString() + "\r\n\r\n";

        }
        #endregion

        
        /// <summary>
        /// Start of Two-Way Anova Region
        /// </summary>


        // Definisi variabel-variabel yang digunakan dalam analisis
        // Two-Way Anova

        Matrix Data2;           // Data treatment dan blok
        Matrix Mu;              // Data Mu
        Matrix Treatment;       // Data Treatment
        Matrix Block;           // Data Blok
        Matrix TWError;         // Data Error

        Matrix TWYMu;           // Data Y-Mu
        Matrix TWYMuSq;         // Data Y-Mu Square
        Matrix TWTreatmentSq;   // Data Treatment Square
        Matrix TWBlockSq;       // Data Blok Square
        Matrix TWErrorSq;       // Data Error Square

        List<Double> TMean = new List<Double>(); // Kumpulan Rata-rata tiap Treatment
        List<Double> BMean = new List<Double>(); // Kumpulan Rata-rata tiap Blok
        Double GrandMeanTW = 0; // Grand Mean dari keseluruhan data

        #region Initial Value Two-Way Anova
        /// <summary>
        /// Method untuk inisialisasi nilai data dan variabel yang digunakan
        /// pada proses kalkulasi analisis Two-Way Anova
        /// </summary>
        private void InitialValue2()
        {
            udTreatment.Value = 3;
            udBlock.Value = 4;

            // Set nilai default dari data treatment untuk tiap blok
            Data2 = new Matrix((Int32)udBlock.Value, (Int32)udTreatment.Value);
            List<Double[]> row = new List<Double[]>();
            row.Add(new Double[] { 70, 65, 71 });
            row.Add(new Double[] { 68, 62, 55 });
            row.Add(new Double[] { 58, 80, 52 });
            row.Add(new Double[] { 65, 67, 60 });

            // Fill matrix with default data
            for (Int32 i = 0; i< row.Count; i++)
            {
                for (Int32 j = 0; j < row[i].Length; j++)
                {
                    Data2[i, j] = row[i][j];
                }
            }

            // Tampilkan matrix datanya
            ShowMatrix(Data2);
        }
        #endregion

        #region ShowMatrix(Matrix m) Display Matrix to DataGridView
        /// <summary>
        /// Method untuk menampilkan matrix m pada group box Debug Message
        /// </summary>
        /// <param name="m">Matrix yang ditampilkan</param>
        private void ShowMatrix(Matrix m)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            // prepare jumlah kolom yang akan ditampilkan
            // ke dalam DataSet
            for (Int32 i = 0; i < udTreatment.Value; i++)
            {
                DataColumn dc = new DataColumn(i.ToString());
                dt.Columns.Add(dc);
            }

            // masukkan nilai baris dan kolom ke dalam DataSet
            for (Int32 i = 0; i < m.NoRows; i++)
            {
                List<Object> col = new List<Object>();
                for (Int32 j = 0; j < m.NoCols; j++)
                {
                    Double val = m[i, j];
                    col.Add(val.ToString());
                }
                dt.Rows.Add(col.ToArray());
            }
            
            // Prepare DataSet
            ds.Tables.Add(dt);

            // Prepare label untuk DataSet
            for (Int32 i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                Int32 index = i+1;
                ds.Tables[0].Columns[i].ColumnName = "T" + index.ToString();
            }

            // Tampilkan matrix pada DataGridView
            dgData2.DataSource = ds.Tables[0];
            for (Int32 i = 0; i < dgData2.Columns.Count; i++) dgData2.Columns[i].Width = 50;

        }
        #endregion

        #region DataGrid Modifier
        /// <summary>
        /// Event method untuk mengubah nilai dimensi matrix data blok dan treatment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// perubahan dimensi Blok
        private void udBlock_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Matrix temp;
                // Jika dimensi blok diperbesar, copy nilai data ke matrix yang baru
                if (udBlock.Value > Data2.NoRows)
                {
                    temp = new Matrix(Data2.NoRows + 1, Data2.NoCols);
                    for (Int32 i = 0; i < Data2.NoRows; i++)
                    {
                        for (Int32 j = 0; j < Data2.NoCols; j++)
                        {
                            temp[i, j] = Data2[i, j];
                        }
                    }
                }
                // Jika dimensi blok diperkecil, copy nilai data ke matrix yang baru
                else
                {
                    temp = new Matrix(Data2.NoRows - 1, Data2.NoCols);
                    for (Int32 i = 0; i < temp.NoRows; i++)
                    {
                        for (Int32 j = 0; j < Data2.NoCols; j++)
                        {
                            temp[i, j] = Data2[i, j];
                        }
                    }
                }
                ShowMatrix(temp);
                Data2 = temp;
            }
            catch (Exception ex) { };
        }

        /// perubahan dimensi Treatment
        private void udTreatment_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // Matrix sementara untuk menampung data matrix dengan dimensi yang baru
                Matrix temp;
                // Jika dimensi Treatment diperbesar, copy nilai data ke matrix yang baru
                if (udTreatment.Value > Data2.NoCols)
                {
                    temp = new Matrix(Data2.NoRows, Data2.NoCols + 1);
                    for (Int32 i = 0; i < Data2.NoRows; i++)
                    {
                        for (Int32 j = 0; j < Data2.NoCols; j++)
                        {
                            temp[i, j] = Data2[i, j];
                        }
                    }
                }
                // Jika dimensi Treatment diperkecil, copy nilai data ke matrix yang baru
                else
                {
                    temp = new Matrix(Data2.NoRows, Data2.NoCols - 1);
                    for (Int32 i = 0; i < Data2.NoRows; i++)
                    {
                        for (Int32 j = 0; j < temp.NoCols; j++)
                        {
                            temp[i, j] = Data2[i, j];
                        }
                    }
                }
                // Tampilkan matrixnya
                ShowMatrix(temp);
                // Simpan matrix dengan dimensi yang baru menjadi matrix data
                Data2 = temp;
            }
            catch { };
        }
        #endregion

        #region Perubahan Nilai Data pada DataGrid
        /// <summary>
        /// Event method untuk mengupdate data yang diubah melalui DataGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgData2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // Parse nilai barunya
                String cellValue = (String)dgData2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                Double NewValue = Double.Parse(cellValue);

                // Update nilai lama menjadi nilai baru
                Data2[e.RowIndex, e.ColumnIndex] = NewValue;
            }
            catch { }
            
            // Tampilkan nilai datanya
            ShowMatrix(Data2);
        }
        #endregion

        /// <summary>
        /// Event Method ketika tombol Proses untuk analisis Two-Way Anova diklik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProses2_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();

            Int32 i = 0;
            Int32 j = 0;
            Double sum = 0;
            Double grandsum = 0;

            try
            {
                // tampilkan matrix data
                Debug(Data2, "Data");

                // Calculate Treatment Data Mean
                for (j = 0; j < Data2.NoCols; j++)
                {
                    sum = 0;
                    for (i = 0; i < Data2.NoRows; i++)
                    {
                        sum += Data2[i, j];
                    }
                    TMean.Add(sum / Data2.NoRows);
                }
                Debug("T Mean", TMean.ToArray());
                 
                // Calculate Block Data Mean and GrandSum
                grandsum = 0;
                for (i = 0; i < Data2.NoRows; i++)
                {
                    sum = 0;
                    for (j = 0; j < Data2.NoCols; j++)
                    {
                        sum += Data2[i, j];
                        grandsum += Data2[i, j];
                    }
                    BMean.Add(sum / Data2.NoCols);
                }
                Debug("B Mean", BMean.ToArray());

                // Calculate Grand Mean
                GrandMeanTW = grandsum / (Data2.NoCols * Data2.NoRows);
                Debug("Grand Mean", GrandMeanTW.ToString());

                // Calculate Matrix Mu
                Mu = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < Data2.NoRows; i++)
                {
                    for (j = 0; j < Data2.NoCols; j++)
                    {
                        Mu[i, j] = GrandMeanTW;
                    }
                }
                Debug(Mu, "Matrix Mu");

                // Calculate Matrix Treatment
                Treatment = new Matrix(Data2.NoRows, Data2.NoCols);
                for (j = 0; j < Data2.NoCols; j++)
                {
                    for (i = 0; i < Data2.NoRows; i++)
                    {
                        Treatment[i, j] = TMean[j] - GrandMeanTW;
                    }
                }
                Debug(Treatment, "Matrix Treatment");

                // Calculate Matrix Block
                Block = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < Data2.NoRows; i++)
                {
                    for (j = 0; j < Data2.NoCols; j++)
                    {
                        Block[i, j] = BMean[i] - GrandMeanTW;
                    }
                }
                Debug(Block, "Matrix Block");

                // Calculate Matrix Error
                TWError = new Matrix(Data2.NoRows, Data2.NoCols);
                TWError = Data2 - (Mu + Treatment + Block);
                Debug(TWError, "Matrix Error");

                // Calculate Matrix Y-Mu Square
                TWYMu = Data2 - Mu;
                TWYMuSq = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < TWYMu.NoRows; i++)
                {
                    for (j = 0; j < TWYMu.NoCols; j++)
                    {
                        TWYMuSq[i, j] = TWYMu[i, j] * TWYMu[i, j];
                    }
                }
                Debug(TWYMuSq, "Matrix Y-Mu Square");

                // Calculate Matrix Treatment Square
                TWTreatmentSq = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < TWTreatmentSq.NoRows; i++)
                {
                    for (j = 0; j < TWTreatmentSq.NoCols; j++)
                    {
                        TWTreatmentSq[i, j] = Treatment[i, j] * Treatment[i, j];
                    }
                }
                Debug(TWTreatmentSq, "Matrix Treatment Square");

                // Calculate Matrix Block Square
                TWBlockSq = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < TWBlockSq.NoRows; i++)
                {
                    for (j = 0; j < TWBlockSq.NoCols; j++)
                    {
                        TWBlockSq[i, j] = Block[i, j] * Block[i, j];
                    }
                }
                Debug(TWBlockSq, "Matrix Block Square");

                // Calculate Matrix Error Square
                TWErrorSq = new Matrix(Data2.NoRows, Data2.NoCols);
                for (i = 0; i < TWErrorSq.NoRows; i++)
                {
                    for (j = 0; j < TWErrorSq.NoCols; j++)
                    {
                        TWErrorSq[i, j] = TWError[i, j] * TWError[i, j];
                    }
                }
                Debug(TWErrorSq, "Matrix Error Square");

                // Calculate Sum Square of Matrix Y-Mu Square
                sum = SumSquare(TWYMuSq);
                Debug("Y-Mu Sum Square", sum.ToString());

                // Calculate Sum Square of Matrix Treatment Square
                sum = SumSquare(TWTreatmentSq);
                Debug("Treatment Sum Square", sum.ToString());

                // Calculate Sum Square of Matrix Block Square
                sum = SumSquare(TWBlockSq);
                Debug("Block Sum Square", sum.ToString());

                // Calculate Sum Square of Matrix Error Square
                sum = SumSquare(TWErrorSq);
                Debug("Error Sum Square", sum.ToString());

                // Calculate Degree of Freedom Y-Mu
                Int32 TWDFYMu = (Data2.NoCols * Data2.NoRows) - 1;
                Debug("Degree of Freedom Y-Mu", TWDFYMu.ToString());

                // Calculate Degree of Freedom Treatment Data
                Int32 TWDFTreatment = Treatment.NoCols - 1;
                Debug("Degree of Freedom Treatment", TWDFTreatment.ToString());

                // Calculate Degree of Freedom Block Data
                Int32 TWDFBlock = Block.NoRows - 1;
                Debug("Degree of Freedom Block", TWDFBlock.ToString());

                // Calculate Degree of Freedom Error
                Int32 TWDFError = TWDFYMu - (TWDFTreatment + TWDFBlock);
                Debug("Degree of Freedom Error", TWDFError.ToString());

                // Calculate Mean Square
                Double MSTreatment = SumSquare(TWTreatmentSq) / TWDFTreatment; // Treatment
                Double MSBlock = SumSquare(TWBlockSq) / TWDFBlock; // Block
                Double MSError = SumSquare(TWErrorSq) / TWDFError; // Error

                Debug("Mean Square Treatment", MSTreatment);
                Debug("Mean Square Block", MSBlock);
                Debug("Mean Square Error", MSError);

                // Calculate F Treatment
                Double FTreatment = MSTreatment / MSError;
                
                // Calculate F Block
                Double FBlock = MSBlock / MSError;
                Debug("F Treatment", FTreatment);
                Debug("F Block", FBlock);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #region Sum Square Function from given Matrix
        /// <summary>
        /// Function to calculate Sum Square from given Matrix
        /// </summary>
        /// <param name="m">Matrix yang dihitung Sum Square-nya</param>
        /// <returns>Nilai Sum Square matrix m</returns>
        private Double SumSquare(Matrix m)
        {
            Double sum = 0;
            for (Int32 i = 0; i < m.NoRows; i++)
            {
                for (Int32 j = 0; j < m.NoCols; j++)
                {
                    sum += m[i, j];
                }
            }
            return sum;
        }
        #endregion

        /// <summary>
        /// Event method from Button to reset the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btReset_Click(object sender, EventArgs e)
        {
            InitialValue();
            InitialValue2();
            tbOutput.Clear();
        }

    }
}